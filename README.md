# VIR

> Implementation of VIR concept


# VIR Concept

The Concept is described in [the wiki section](./wiki/README.md).


# Live Demo

[https://chcemywir.pl/](https://chcemywir.pl/)


# Project Overview

The project directory is mostly self-explanatory

```shell
tree -L 1
.
├── backend
├── frontend
├── readme.md
└── wiki
```

1. backend includes backend code for VIR
2. frontend includes frontend code for VIR
3. wiki includes knowledge base


# Roadmap Overview

1. a few iteration of feature development
2. Improving code quality; tests, CI, and many more
3. create a legal fully working software almost certainly based on blockchain
4. security audit
