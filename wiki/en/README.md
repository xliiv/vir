# VIR Concept

VIR is one of approaches to
[direct democracy](https://en.wikipedia.org/wiki/Direct_democracy).

VIR defines 3 tools for citizens

1. Veto
2. Initiative
3. Referendum

## Veto

Citizens remove existing legislation, more on
[Wikipedia](https://en.wikipedia.org/wiki/Popular_referendum)

## Initiative

Citizens create new legislation, more on
[Wikipedia](https://en.wikipedia.org/wiki/Initiative)

## Referendum

Citizens vote on a particular legislation proposal.

The proposal may be

1. the legislation being under Veto
2. the legislation defined by Initiative

more on [Wikipedia](https://en.wikipedia.org/wiki/Referendum)


# Frequently Asked Questions and Others

1. Why are you doing it?

    I see sense in this and profit for people.

    Imagine that MPs introduce a bad law for people.

    Now, you can only get nervous or ignore it. With `VIR` you will remove the bad law.

2. Great idea, but "It will not succeed"

    You see a problem or opportunities? I see the opportunities and I make it work.

3. Great idea, but "some things don't work"

    You see a problem or opportunities? I see the opportunities to fix it.

4. Great idea, but "it's not legal"

    You see a problem or opportunities? I see the opportunities to change the law.

5. This is great, can I help somehow?

    * Tell your family and friends about `VIR`
    * Show `VIR` your family and friends
    * Ask if they like it

6. Where is the code?

    https://gitlab.com/xliiv/vir/

7. What's Next?

    The evolution of the current preview version of the application to the
    legal target on [blockchain](https://en.wikipedia.org/wiki/Blockchain).
