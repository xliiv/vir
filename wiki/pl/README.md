# Koncepcja WIR

WIR jest jednym z podejść do [Demokracji
Bezpośredniej](https://pl.wikipedia.org/wiki/Demokracja_bezpo%C5%9Brednia).

WIR określa 3 narzędzia dla obywateli

1. Weto
2. Inicjatywa
3. Referendum

## Weto

Obywatele usuwają istniejące prawo, więcej na
[Wikipedia](https://pl.wikipedia.org/wiki/Weto_ludowe).

## Inicjatywa

Obywatele wprowadzają nowe prawo, więcej na
[Wikipedia](https://pl.wikipedia.org/wiki/Inicjatywa_obywatelska).

## Referendum

Obywatele zatwierdzają konkretną zmiany prawa.

Tą zmianą może być

1. usunięciem przepisów ustalnoych na drodze Weta
2. dodaniem przepisów ustalnoych na drodze Inicjatywy

więcej na [Wikipedia](https://pl.wikipedia.org/wiki/Referendum).


# Pytania i Inne

1. Czemu to robisz?

    Widzę w tym sens i zysk dla ludzi.

    Wyobraż sobie, że posłowie wprowadzają złą ustawę dla ludzi, np

    1. bezkarność+
    2. podwyżki dla posłów

    Obecnie, możesz się tylko denerwować lub olać. Dzięki `WIR` usuniesz złą
    ustawę.

2. Fajny pomysł, ale "to się nie uda"

    Widzisz przyszkody czy okazje? Ja widzę okazję i sprawiam, że się udaje.

3. Fajny pomysła, ale "część nie działa"

    Widzisz przyszkode czy okazje? Ja widzę okazję do naprawy.

4. Fajny pomysła, ale "to nie zgodne z prawem"

    Widzisz przyszkody czy okazje? Ja widzę okazję na zmienimę prawa.

5. To jest super, mogę pomóc?

    * Opowiedz o `WIR` bliskim i dalekim.
    * Pokaż `WIR` bliskim i dalekim.
    * Spytaj czy pomysł się podoba.

6. Gdzie jest kod projektu?

    https://gitlab.com/xliiv/vir/

7. Jakie następne kroki?

    Ewolucja obecnej poglądowej wersji aplikacji do legalnej docelowej (pewnie)
    na [blockchain](https://pl.wikipedia.org/wiki/Blockchain).
