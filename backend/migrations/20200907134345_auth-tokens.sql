-- stores tmp tokens send to user email for auth purpose
-- these tokens are swapped with real auth tokens
CREATE TABLE token_promises (
    id SERIAL NOT NULL PRIMARY KEY,
    content text NOT NULL,
    created_at TIMESTAMPTZ default (now() at time zone 'utc') NOT NULL,
    invalid boolean default false NOT NULL,
    user_id int REFERENCES users (id) ON UPDATE CASCADE NOT NULL,
    UNIQUE (content, user_id)
);

-- stores auth tokens which got invalidated (like when user logs out)
CREATE TABLE invalidated_auth_tokens (
    id SERIAL NOT NULL PRIMARY KEY,
    content text NOT NULL UNIQUE,
    created_at TIMESTAMPTZ default (now() at time zone 'utc') NOT NULL
);
