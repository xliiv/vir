CREATE TABLE veto_votes (
    id SERIAL NOT NULL PRIMARY KEY,
    act_id text NOT NULL,
    user_id int REFERENCES users (id) ON UPDATE CASCADE NOT NULL,
    created_at TIMESTAMPTZ default (now() at time zone 'utc') NOT NULL,
    UNIQUE (act_id, user_id)
);
