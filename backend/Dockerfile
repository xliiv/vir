FROM rust as build
RUN apt-get update; \
    apt-get install --no-install-recommends --yes libpq-dev; \
    rm -rf /var/lib/apt/lists/*; \
    USER=root cargo new --bin app

RUN cargo install --version=0.1.0-beta.1 sqlx-cli --no-default-features --features postgres

WORKDIR /app
COPY ./Cargo.toml ./Cargo.toml
RUN cargo build --release; \
    rm src/*.rs

COPY ./i18n.toml ./i18n.toml
COPY ./i18n ./i18n
COPY ./sqlx-data.json ./sqlx-data.json
COPY ./src ./src
RUN rm ./target/release/deps/vir*; \
    cargo build --release


FROM debian:buster
WORKDIR /app
RUN apt-get update; \
    apt-get install --no-install-recommends --yes \
        ca-certificates \
        libpq-dev \
        netcat; \
    rm -rf /var/lib/apt/lists/*;

COPY ./sqlx-data.json ./sqlx-data.json
COPY ./migrations ./migrations
COPY --from=build /app/target/release/vir .
COPY --from=build /usr/local/cargo/bin/sqlx .

EXPOSE 8000
