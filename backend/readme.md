# Running

Since `vir` uses `query!` from `sqlx` you have to provide a running DB during
compilation. However, if you don't want to, you can skip it be `SQLX_OFFLINE`
env. var. For example

```shell
SQLX_OFFLINE='true' cargo check
```

# DB

A few handful commands needed for dev of this project.

## Running DB

```shell
docker run --network=host --name vir -e POSTGRES_PASSWORD=password postgres
# or if we want to auto-delete container
docker run --rm --network=host --name vir -e POSTGRES_PASSWORD=password postgres
```

## Init DB

```shell
env DATABASE_URL="postgres://postgres:password@localhost/vir" sqlx db create
env DATABASE_URL="postgres://postgres:password@localhost/vir" sqlx migrate run
```

## Inspect DB

```shell
# review migration list
env DATABASE_URL="postgres://postgres:password@localhost/vir" sqlx migrate info
```

## Migrate DB

```shell
env DATABASE_URL="postgres://postgres:password@localhost/vir" sqlx migrate run
```

## How to Add a New Model

1. Generate migration file

```shell
sqlx migrate add <migration-name>
```

2. Write content of the created migration file
```shell
vim backend/migrations/<time>_<migration-name>.sql
```


## Connecting database

```shell
psql -h localhost -U postgres
# or
psql -h 172.19.0.2 -U postgres
# then
\c vir
```

https://github.com/testdrivenio/django-on-docker/blob/master/docker-compose.prod.yml


## Sqlx Offline Mode

See https://github.com/launchbadge/sqlx/tree/master/sqlx-cli#enable-building-in-offline-mode-with-query

### Generating `sqlx-data.json`
```shell
cargo sqlx prepare -- --lib
```

### Checking `sqlx-data.json`
```shell
cargo sqlx prepare --check -- --lib
```

# Docker Image

```shell
docker build . -t vir-backend
```
