#!/usr/bin/env bash
set -ex

DB_CONTAINER_NAME=vir-test

docker rm "$DB_CONTAINER_NAME" --force || true

docker run \
    --detach \
    --rm \
    --env POSTGRES_PASSWORD=password \
    --env POSTGRES_USER=postgres \
    --publish 5432:5432 \
    --name "$DB_CONTAINER_NAME" \
    postgres

echo waiting for DB
sleep 3

sqlx db create
sqlx migrate run
cargo test $1 -- --nocapture

docker stop "$DB_CONTAINER_NAME"
