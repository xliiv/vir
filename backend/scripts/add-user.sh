#!/usr/bin/env bash

set -ex

sqlx database drop
sqlx database create
sqlx migrate run


// TODO: mv it to suitable place like scripts/test_user.sh or something
curl -v -H "Content-Type: application/json"  -X POST \
    -d '{"email": "foo1@bar.com"}' \
    127.0.0.1:8000/users
