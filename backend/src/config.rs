use actix_cors::Cors;
use actix_web::http::header;
use lazy_static::lazy_static;
use serde::Deserialize;

pub fn cors() -> actix_cors::CorsFactory {
    let cors_domain = std::env::var("VIR_CORS_DOMAIN").expect("VIR_CORS_DOMAIN must be set");
    let mut cors = Cors::new()
        .allowed_methods(vec!["GET", "POST", "DELETE"])
        .allowed_header(header::ACCEPT)
        .allowed_header(header::AUTHORIZATION)
        .allowed_header(header::CONTENT_TYPE)
        .max_age(3600);

    if cors_domain != "*" {
        for domain in cors_domain.split(',') {
            cors = cors.allowed_origin(domain);
        }
    }
    cors.finish()
}

#[derive(Deserialize, Debug)]
pub struct Config {
    pub app_address: String,
    pub backend_socket: String,
    pub database_url: String,
    pub rust_log: String,
    pub secret: String,
    pub email_username: String,
    pub email_password: String,
    pub smtp_server: String,
    pub skip_email: bool,
}

#[cfg(not(test))]
const CONFIG_PATH: &str = ".env";
#[cfg(not(test))]
fn init_logger() {
    env_logger::init();
}

#[cfg(test)]
fn init_logger() {
    let _ = env_logger::builder().is_test(true).try_init();
}
#[cfg(test)]
const CONFIG_PATH: &str = ".env.test";

fn init_config() -> Config {
    dotenv::from_filename(CONFIG_PATH).ok();
    let config = envy::from_env::<Config>().expect("Can't parse dotenv");

    // set up logger
    std::env::set_var("RUST_LOG", &config.rust_log);
    init_logger();

    config
}

lazy_static! {
    pub static ref CONFIG: Config = init_config();
}
