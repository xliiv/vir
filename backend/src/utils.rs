use lettre::message::SinglePart;
use lettre::message::header;
use lettre::transport::smtp::authentication::Credentials;
use lettre::{Message, SmtpTransport, Transport};
use log::{debug, info};

use crate::config;

pub fn send_email(
    recipient: &str,
    subject: &str,
    body: &str,
    dry_run: bool,
) -> Result<(), Box<dyn std::error::Error>> {
    let email = Message::builder()
        .header(header::ContentType::text_utf8())
        .from(config::CONFIG.email_username.parse()?)
        .reply_to(config::CONFIG.email_username.parse()?)
        .to(recipient.parse()?)
        .subject(subject)
        .singlepart(
            SinglePart::builder()
                .header(header::ContentType::text_utf8())
                .header(header::ContentTransferEncoding::QuotedPrintable)
                .body(body),
        )?;
    debug!("{:?}", email);

    let creds = Credentials::new(
        config::CONFIG.email_username.clone(),
        config::CONFIG.email_password.clone(),
    );
    let mailer = SmtpTransport::starttls_relay(&config::CONFIG.smtp_server)?
        .credentials(creds)
        .build();
    if dry_run {
        let msg = format!(
            "Recipient: {}\nSubject:{}\nBody:{}\n",
            &recipient, &subject, &body
        );
        info!("DRY-RUN ENABLED, skipping mail with data\n{}", msg);
    } else {
        mailer.send(&email)?;
    }
    Ok(())
}

#[cfg(test)]
mod send_emails {
    use super::*;
    use lettre::address::AddressError;

    #[test]
    fn test_gives_error_if_bad_recipient() {
        let result = send_email("", "Hello", "Hello from test email", true);

        let boxed = result.expect_err("Expected recipient error");
        let error = boxed.downcast_ref::<AddressError>().unwrap();
        assert!(matches!(error, AddressError::MissingParts));
    }

    #[test]
    fn sends_for_utf8() {
        send_email(
            "test@example.com",
            "title-żźćąśęółńŻŹĆĄŚĘÓŁŃ",
            "title-żźćąśęółńŻŹĆĄŚĘÓŁŃ",
            true,
        )
        .expect("email should be sent with dry-run");
    }
}
