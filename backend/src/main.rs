extern crate log;
use actix_web::{middleware, web, App, HttpServer};
use actix_web_httpauth::middleware::HttpAuthentication;
use sqlx::PgPool;

use vir::auth;
use vir::config;
use vir::handlers;

#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    let db_pool = PgPool::connect(&config::CONFIG.database_url).await
        .expect("Set DATABASE_URL env variable pointing at postgres db");

    println!("Serving at http://{}", &config::CONFIG.backend_socket);

    HttpServer::new(move || {
        App::new()
            .data(db_pool.clone())
            .wrap(middleware::Logger::default())
            .wrap(config::cors())
            .service(
                // RESTRICTED VIEWS - authentication required
                web::scope("/api")
                    .wrap(HttpAuthentication::bearer(auth::bearer_validator))
                    .route("/veto", web::post().to(handlers::veto))
                    .route("/user-veto", web::get().to(handlers::user_veto_get))
                    .route("/user-veto", web::delete().to(handlers::user_veto_delete)),
            )
            // PUBLIC VIEWS - anyone can request them
            .route("/auth/join", web::post().to(handlers::request_join))
            .route(
                "/auth/exchange-token",
                web::post().to(handlers::exchange_token),
            )
            .route("/auth/leave", web::post().to(handlers::leave))
    })
    .bind(&config::CONFIG.backend_socket)
    .expect("Can't initialize HttpServer")
    .run()
    .await
}
