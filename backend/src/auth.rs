use actix_web::dev::ServiceRequest;
use actix_web::Error;
use actix_web_httpauth::extractors::bearer::{BearerAuth, Config};
use actix_web_httpauth::extractors::AuthenticationError;
use chrono::prelude::*;
use jsonwebtoken::{decode, DecodingKey, EncodingKey, Header, Validation};
use serde::{Deserialize, Serialize};

use crate::config;

#[derive(Debug, PartialEq, Deserialize, Serialize)]
pub struct Claims {
    pub exp: i64,
    pub iat: i64,
    pub sub: String,
}

pub fn validate_token(token: &str) -> bool {
    match decode::<Claims>(
        &token,
        &DecodingKey::from_secret(&config::CONFIG.secret.as_bytes()),
        &Validation::default(),
    ) {
        Ok(_) => {
            return true;
        }
        Err(e) => {
            eprintln!("{:?}", e);
            return false;
        }
    }
}

pub fn generate_jwt<T: AsRef<str>>(user_id: T) -> Result<String, Box<dyn std::error::Error>> {
    let iat = Utc::now();
    let exp = iat + chrono::Duration::days(1);
    let claims = Claims {
        exp: exp.timestamp(),
        iat: iat.timestamp(),
        sub: user_id.as_ref().into(),
    };
    let token = jsonwebtoken::encode(
        &Header::default(),
        &claims,
        &EncodingKey::from_secret(&config::CONFIG.secret.as_bytes()),
    )?;
    return Ok(token);
}

/// Used by Authentication mechanism
pub async fn bearer_validator(
    request: ServiceRequest,
    credentials: BearerAuth,
) -> Result<ServiceRequest, Error> {
    if validate_token(credentials.token()) {
        Ok(request)
    } else {
        let config = request
            .app_data::<Config>()
            .map(|data| data.clone())
            .unwrap_or_else(Default::default);
        Err(AuthenticationError::from(config).into())
    }
}

pub fn token2id(token: &str) -> jsonwebtoken::errors::Result<String> {
    let token_data = decode::<Claims>(
        &token,
        &DecodingKey::from_secret(&config::CONFIG.secret.as_bytes()),
        &Validation::default(),
    )?;
    Ok(token_data.claims.sub)
}

#[cfg(test)]
mod generate_token {
    use super::*;

    #[test]
    fn generate_token_works() {
        let secret = "xxxx";
        let id = 1.to_string();

        let token = generate_jwt(&id).unwrap();

        let validation = Validation {
            sub: Some(id.clone()),
            ..Validation::default()
        };
        let token_data = decode::<Claims>(
            &token,
            &DecodingKey::from_secret(secret.as_ref()),
            &validation,
        )
        .unwrap();
        assert_eq!(&token_data.claims.sub, &id);
    }
}

#[cfg(test)]
mod token2id {
    use super::*;

    #[test]
    fn works_for_correct_token() {
        let user_id = 1;
        let token = generate_jwt(user_id.to_string()).unwrap();

        let found_id = token2id(&token);

        assert_eq!(found_id.unwrap(), user_id.to_string())
    }
}
