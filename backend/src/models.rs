use anyhow::Result;
use serde::Serialize;
use sqlx;
use sqlx::prelude::Done;
use sqlx::PgPool;
use uuid::Uuid;

#[derive(Debug, Serialize)]
pub struct VetoVote {
    pub id: i32,
    pub act_id: String,
    pub user_id: i32,
    pub created_at: chrono::DateTime<chrono::Utc>,
}

impl VetoVote {
    pub async fn create(
        user_id: i32,
        act_id: &str,
        pool: &PgPool,
    ) -> std::result::Result<Self, sqlx::Error> {
        let mut tx = pool.begin().await?;

        let veto_vote = sqlx::query_as!(
            VetoVote,
            r#"
            INSERT INTO veto_votes (user_id, act_id)
            VALUES ($1, $2)
            RETURNING id, user_id, act_id, created_at
            "#,
            user_id,
            act_id,
        )
        .fetch_one(&mut tx)
        .await?;

        tx.commit().await?;

        Ok(veto_vote)
    }

    pub async fn user_votes(
        user_id: i32,
        pool: &PgPool,
    ) -> std::result::Result<Vec<Self>, sqlx::Error> {
        // TODO: act_ids are skipped because of
        // https://github.com/launchbadge/sqlx/issues/714
        // maybe can be fixed with
        // https://github.com/launchbadge/sqlx/issues/656#issuecomment-699638262
        // https://github.com/launchbadge/sqlx/issues/350#issuecomment-636249259
        // https://github.com/launchbadge/sqlx/issues/528#issuecomment-659861391
        sqlx::query_as!(
            VetoVote,
            r#"
            SELECT *
            FROM veto_votes
            WHERE user_id = $1
            "#,
            user_id,
        )
        .fetch_all(pool)
        .await
    }

    pub async fn delete(
        user_id: i32,
        act_id: &str,
        pool: &PgPool,
    ) -> std::result::Result<u64, sqlx::Error> {
        let mut tx = pool.begin().await?;

        let deleted = sqlx::query!(
            r#"
            DELETE FROM veto_votes
            WHERE
                user_id = $1
                AND act_id = $2
            "#,
            user_id,
            act_id,
        )
        .execute(&mut tx)
        .await?;

        tx.commit().await?;

        Ok(deleted.rows_affected())
    }
}

#[derive(Debug, Serialize)]
pub struct InvalidatedAuthTokens {
    pub id: i32,
    pub content: String,
    pub created_at: chrono::DateTime<chrono::Utc>,
}

impl InvalidatedAuthTokens {
    pub async fn create(token: &str, pool: &PgPool) -> std::result::Result<Self, sqlx::Error> {
        let mut tx = pool.begin().await?;
        let banned = sqlx::query_as!(
            InvalidatedAuthTokens,
            r#"
            INSERT INTO invalidated_auth_tokens (content)
            VALUES ($1)
            RETURNING id, content, created_at
            "#,
            token,
        )
        .fetch_one(&mut tx)
        .await?;
        tx.commit().await?;
        Ok(banned)
    }

    pub async fn get(token: &str, pool: &PgPool) -> std::result::Result<Self, sqlx::Error> {
        let found = sqlx::query_as!(
            InvalidatedAuthTokens,
            r#"
            SELECT id, content, created_at
            FROM invalidated_auth_tokens
            WHERE content = $1
            "#,
            token,
        )
        .fetch_one(pool)
        .await?;
        Ok(found)
    }
}

#[derive(Serialize)]
pub struct TokenPromise {
    pub id: i32,
    pub content: String,
    pub created_at: chrono::DateTime<chrono::Utc>,
    pub invalid: bool,
    pub user_id: i32,
}

impl TokenPromise {
    pub async fn create(user_id: i32, pool: &PgPool) -> Result<Self> {
        let uuid = Uuid::new_v4();

        let mut tx = pool.begin().await?;

        let token_promise = sqlx::query_as!(
            TokenPromise,
            r#"
            INSERT INTO token_promises (content, user_id)
            VALUES ($1, $2)
            RETURNING id, content, created_at, invalid, user_id
            "#,
            &uuid.to_string(),
            user_id,
        )
        .fetch_one(&mut tx)
        .await?;

        tx.commit().await?;

        Ok(token_promise)
    }

    pub async fn get(
        user_id: i32,
        token: &str,
        pool: &PgPool,
    ) -> std::result::Result<Self, sqlx::Error> {
        sqlx::query_as!(
            TokenPromise,
            r#"
            SELECT id, content, created_at, invalid, user_id
            FROM token_promises
            WHERE
                user_id = ($1)
                AND content = ($2)
            "#,
            user_id,
            token,
        )
        .fetch_one(pool)
        .await
    }

    pub async fn invalidate_token(user_id: i32, token: &str, pool: &PgPool) -> Result<()> {
        let mut tx = pool.begin().await?;
        sqlx::query!(
            r#"
            UPDATE token_promises
            SET invalid = true
            WHERE user_id = $1 AND content = $2
            "#,
            user_id,
            token,
        )
        .execute(&mut tx)
        .await?;

        tx.commit().await?;

        Ok(())
    }
}

#[derive(Debug, Serialize)]
pub struct User {
    pub id: i32,
    pub email: String,
    pub created_at: chrono::DateTime<chrono::Utc>,
}

impl User {
    pub async fn get(id: i32, pool: &PgPool) -> sqlx::Result<Option<User>> {
        sqlx::query_as!(User, r#"SELECT * FROM users WHERE id = $1 "#, id)
            .fetch_optional(pool)
            .await
    }

    pub async fn get_by_email(email: &str, pool: &PgPool) -> sqlx::Result<Option<User>> {
        let user_opt = sqlx::query_as!(User, r#"SELECT * FROM users WHERE email = $1 "#, email,)
            .fetch_optional(pool)
            .await?;
        Ok(user_opt)
    }

    /// Creates a user.
    pub async fn create(user_email: &str, pool: &PgPool) -> Result<User> {
        let mut tx = pool.begin().await?;

        let user = sqlx::query_as!(
            User,
            r#"
            INSERT INTO users (email)
            VALUES ($1) RETURNING id, email, created_at
            "#,
            &user_email,
        )
        .fetch_one(&mut tx)
        .await?;

        tx.commit().await?;
        Ok(user)
    }
}
