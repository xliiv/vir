use actix_web::{web, HttpResponse, Responder};
use actix_web_httpauth::extractors::bearer::BearerAuth;
use i18n_embed::fluent::FluentLanguageLoader;
use i18n_embed_fl::fl;
use sqlx;
use sqlx::PgPool;

use super::ApiResult;
use super::Status;
use crate::auth;
use crate::i18n;
use crate::models;

use serde::{Deserialize, Serialize};
#[derive(Debug, Deserialize, Serialize)]
pub struct VetoGet {
    pub act_id: String,
}

pub async fn veto(
    wrapper: i18n::LoaderWrapper,
    get_data: web::Json<VetoGet>,
    db_pool: web::Data<PgPool>,
    auth: BearerAuth,
) -> impl Responder {
    let loader: FluentLanguageLoader = wrapper.into();

    let user_id = match auth::token2id(auth.token()) {
        Ok(user_id_str) => match user_id_str.parse::<i32>() {
            Ok(user_id) => user_id,
            Err(e) => {
                eprintln!("{:?}", e);
                return HttpResponse::BadRequest().json(ApiResult {
                    status: Status::Error,
                    message: fl!(loader, "token-sub-not-int"),
                });
            }
        },
        Err(e) => {
            eprintln!("{:?}", e);
            return HttpResponse::BadRequest().json(ApiResult {
                status: Status::Error,
                message: fl!(loader, "token-invalid"),
            });
        }
    };
    match models::VetoVote::create(user_id, &get_data.act_id, db_pool.as_ref()).await {
        Ok(_) => {}
        Err(sqlx::Error::Database(e)) if e.message().contains("veto_votes_act_id_user_id_key") => {
            return HttpResponse::Conflict().json(ApiResult {
                status: Status::Error,
                message: fl!(
                    loader,
                    "veto-vote-exists",
                    user_id = user_id,
                    act_id = get_data.act_id.as_str()
                ),
            });
        }
        Err(e) => {
            dbg!(&e);
            eprintln!("{:?}", e);
            return HttpResponse::InternalServerError().json(ApiResult {
                status: Status::Error,
                message: fl!(loader, "veto-vote-error"),
            });
        }
    }
    return HttpResponse::Ok().json(ApiResult {
        status: Status::Success,
        message: fl!(loader, "veto-submitted"),
    });
}

#[cfg(test)]
mod veto {
    use super::*;
    use crate::auth;
    use crate::config;
    use actix_web::{http::StatusCode, test, web, App};
    use actix_web_httpauth::middleware::HttpAuthentication;

    // TODO: fix it https://github.com/actix/actix-extras/issues/90
    //#[actix_rt::test]
    //async fn gives_401_if_no_token() {
    //    let mut app = test::init_service(
    //        App::new()
    //            .wrap(HttpAuthentication::bearer(auth::bearer_validator))
    //            .route("/", web::post().to(veto))
    //    ).await;
    //    let req = test::TestRequest::post()
    //        .uri("/")
    //        .to_request();

    //    let response = test::call_service(&mut app, req).await;

    //    dbg!(&response);

    //    //assert_eq!(response.status(), StatusCode::UNAUTHORIZED);
    //}

    #[actix_rt::test]
    async fn gives_200_if_valid_token() {
        let db_pool = PgPool::connect(&config::CONFIG.database_url)
            .await
            .expect("can't connect db");
        let user = models::User::create("ElBlaze@example.com", &db_pool)
            .await
            .expect("can't create user");
        let token = auth::generate_jwt(user.id.to_string()).expect("can't generate token");
        let mut app = test::init_service(
            App::new()
                .data(db_pool.clone())
                .wrap(HttpAuthentication::bearer(auth::bearer_validator))
                .route("/", web::post().to(veto)),
        )
        .await;
        let req = test::TestRequest::post()
            .uri("/")
            .header("Authorization", format!("Bearer {}", &token))
            .set_json(&VetoGet {
                act_id: "some-id".to_string(),
            })
            .to_request();

        let response = test::call_service(&mut app, req).await;

        assert_eq!(response.status(), StatusCode::OK);
    }

    #[actix_rt::test]
    async fn gives_200_if_veto_vote_is_missing() {
        let db_pool = PgPool::connect(&config::CONFIG.database_url)
            .await
            .expect("can't connect db");
        let email = "ElBlaze@example.com".to_string();
        let user = models::User::create(&email, &db_pool)
            .await
            .expect("can't create user");
        let token = auth::generate_jwt(user.id.to_string()).expect("can't generate token");
        let mut app = test::init_service(
            App::new()
                .data(db_pool.clone())
                .wrap(HttpAuthentication::bearer(auth::bearer_validator))
                .route("/", web::post().to(veto)),
        )
        .await;
        let req = test::TestRequest::post()
            .uri("/")
            .header("Authorization", format!("Bearer {}", &token))
            .set_json(&VetoGet {
                act_id: "DU/2020/1".to_string(),
            })
            .to_request();

        let response = test::call_service(&mut app, req).await;

        assert_eq!(response.status(), StatusCode::OK);
        let body = test::read_body(response).await;
        let result: ApiResult = serde_json::from_slice(&body)
            .expect("read_response_json failed during deserialization");
        assert_eq!(
            result,
            ApiResult {
                status: Status::Success,
                message: "Veto submitted".to_string(),
            }
        );
    }

    #[actix_rt::test]
    async fn gives_409_if_veto_vote_is_present() {
        let db_pool = PgPool::connect(&config::CONFIG.database_url)
            .await
            .expect("can't connect db");
        let email = "ElBlaze@example.com".to_string();
        let user = models::User::create(&email, &db_pool)
            .await
            .expect("can't create user");
        let act_id = "some-act-id";
        models::VetoVote::create(user.id, &act_id, &db_pool)
            .await
            .unwrap();
        let token = auth::generate_jwt(user.id.to_string()).expect("can't generate token");
        let mut app = test::init_service(
            App::new()
                .data(db_pool.clone())
                .wrap(HttpAuthentication::bearer(auth::bearer_validator))
                .route("/", web::post().to(veto)),
        )
        .await;
        let req = test::TestRequest::post()
            .uri("/")
            .header("Authorization", format!("Bearer {}", &token))
            .set_json(&VetoGet {
                act_id: act_id.to_owned(),
            })
            .to_request();

        let response = test::call_service(&mut app, req).await;

        assert_eq!(response.status(), StatusCode::CONFLICT);
        let body = test::read_body(response).await;
        let result: ApiResult = serde_json::from_slice(&body)
            .expect("read_response_json failed during deserialization");
        assert_eq!(
            result,
            ApiResult {
                status: Status::Error,
                message: format!(
                    "Veto vote exists for user \u{2068}{}\u{2069} and act \u{2068}{}\u{2069}",
                    &user.id, &act_id,
                )
            }
        );
    }
}
