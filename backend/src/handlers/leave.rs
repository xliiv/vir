use super::ApiResult;
use super::Status;
use actix_web::{http, web, Responder};
use actix_web_httpauth::extractors::bearer::BearerAuth;
use i18n_embed::fluent::FluentLanguageLoader;
use i18n_embed_fl::fl;
use sqlx;
use sqlx::PgPool;

use crate::i18n;
use crate::models;

pub async fn leave(
    wrapper: i18n::LoaderWrapper,
    db_pool: web::Data<PgPool>,
    auth: BearerAuth,
) -> impl Responder {
    let loader: FluentLanguageLoader = wrapper.into();
    let id_token = auth.token();
    if let Err(e) = models::InvalidatedAuthTokens::create(&id_token, db_pool.as_ref()).await {
        eprintln!("{:?}", e);
        return ApiResult{
            status: Status::Error,
            message: fl!(loader, "generic-error-msg"),
        }.with_status(http::StatusCode::INTERNAL_SERVER_ERROR);
    };
    return ApiResult {
        status: Status::Success,
        message: fl!(loader, "account-left"),
    }.with_status(http::StatusCode::OK);
}

#[cfg(test)]
mod leave {
    use super::*;
    use crate::config;
    use actix_web::{http::StatusCode, test, web, App};

    #[actix_rt::test]
    async fn gets_200_for_happy_path() {
        let token = "jwt-token";
        let db_pool = PgPool::connect(&config::CONFIG.database_url).await.unwrap();
        let mut app = test::init_service(
            App::new()
                .data(PgPool::connect(&config::CONFIG.database_url).await.unwrap())
                .route("/", web::post().to(leave)),
        )
        .await;
        let req = test::TestRequest::post()
            .uri("/")
            .header("Authorization", format!("Bearer {}", token))
            .to_request();

        let response = test::call_service(&mut app, req).await;

        assert_eq!(response.status(), StatusCode::OK);
        let body = test::read_body(response).await;
        let result: ApiResult = serde_json::from_slice(&body)
            .expect("read_response_json failed during deserialization");
        assert_eq!(
            result,
            ApiResult {
                status: Status::Success,
                message: "Left your account".to_string(),
            }
        );

        let found = models::InvalidatedAuthTokens::get(token, &db_pool)
            .await
            .unwrap();
        assert_eq!(found.content, token);
    }

    // TODO: logout with missing token gives 401 + suitable msg
    // TODO: logout with broken token gives 401 + suitable msg
    // TODO: logout with obsolete token gives 401 + suitable msg
}
