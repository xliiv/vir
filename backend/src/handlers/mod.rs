use actix_web::{Error, HttpRequest, HttpResponse, Responder};
use futures::future::{ready, Ready};
use serde::{Deserialize, Serialize};

mod exchange_token;
mod leave;
mod request_join;
mod user_veto_delete;
mod user_veto_get;
mod veto;
pub use exchange_token::exchange_token;
pub use leave::leave;
pub use request_join::request_join;
pub use user_veto_delete::user_veto_delete;
pub use user_veto_get::user_veto_get;
pub use veto::veto;

#[derive(Debug, Serialize, Deserialize, PartialEq)]
enum Status {
    Success,
    Error,
}

#[derive(Debug, Serialize, Deserialize, PartialEq)]
struct ApiResult {
    status: Status,
    message: String,
}

impl Responder for ApiResult {
    type Error = Error;
    type Future = Ready<Result<HttpResponse, Error>>;
    fn respond_to(self, _req: &HttpRequest) -> Self::Future {
        let body = serde_json::to_string(&self).unwrap();
        ready(Ok(HttpResponse::Ok()
            .content_type("application/json")
            .body(body)))
    }
}
