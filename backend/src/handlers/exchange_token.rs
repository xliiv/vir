use super::ApiResult;
use super::Status;
use actix_web::{web, HttpResponse, Responder};
use chrono::prelude::*;
use i18n_embed::fluent::FluentLanguageLoader;
use i18n_embed_fl::fl;
use serde::{Deserialize, Serialize};
use sqlx;
use sqlx::PgPool;
use validator::Validate;

use crate::auth;
use crate::i18n;
use crate::models;

#[derive(Debug, Serialize, Deserialize, PartialEq)]
struct TokenResult {
    status: Status,
    message: String,
    token: String,
}

#[derive(Debug, Deserialize, Serialize, Validate)]
pub struct ExchangeData {
    pub user_id: i32,
    #[validate(length(equal = 36))]
    pub token: String,
}

pub async fn exchange_token(
    wrapper: i18n::LoaderWrapper,
    exchange_data: web::Json<ExchangeData>,
    db_pool: web::Data<PgPool>,
) -> impl Responder {
    let loader: FluentLanguageLoader = wrapper.into();
    if let Err(e) = exchange_data.validate() {
        eprintln!("{:?}", e);
        return HttpResponse::BadRequest().json(ApiResult {
            status: Status::Error,
            message: fl!(loader, "invalid-exchange-token"),
        });
    }
    let auth_token = match auth::generate_jwt(exchange_data.user_id.to_string()) {
        Ok(token) => token,
        Err(e) => {
            eprintln!("{:?}", e);
            return HttpResponse::InternalServerError().json(ApiResult {
                status: Status::Error,
                message: fl!(loader, "cant-generate-token"),
            });
        }
    };

    // TODO: consider moving logic to the query as much as possible
    // TODO: consider using select and invalidate in one query
    let token_promise = match models::TokenPromise::get(
        exchange_data.user_id,
        &exchange_data.token,
        db_pool.as_ref(),
    )
    .await
    {
        Ok(token) => token,
        Err(sqlx::Error::RowNotFound) => {
            eprintln!("RowNotFound");
            return HttpResponse::BadRequest().json(ApiResult {
                status: Status::Error,
                message: fl!(loader, "promise-data-missing"),
            });
        }
        Err(e) => {
            eprintln!("{:?}", e);
            return HttpResponse::InternalServerError().json(ApiResult {
                status: Status::Error,
                message: fl!(loader, "generic-error-msg"),
            });
        }
    };
    if token_promise.invalid {
        return HttpResponse::BadRequest().json(ApiResult {
            status: Status::Error,
            message: fl!(loader, "token-used"),
        });
    }
    let deadline = token_promise.created_at + chrono::Duration::hours(1);
    if Utc::now() > deadline {
        return HttpResponse::BadRequest().json(ApiResult {
            status: Status::Error,
            message: fl!(loader, "token-obsolete"),
        });
    }
    match models::TokenPromise::invalidate_token(
        exchange_data.user_id,
        &exchange_data.token,
        &db_pool,
    )
    .await
    {
        Ok(_) => {}
        Err(e) => {
            eprintln!("{:?}", e);
            return HttpResponse::InternalServerError().finish();
        }
    };
    let user = match models::User::get(exchange_data.user_id, &db_pool).await {
        Ok(Some(user)) => user,
        Ok(None) => {
            return HttpResponse::BadGateway().json(ApiResult {
                status: Status::Error,
                message: fl!(loader, "cant-get-user"),
            });
        }
        _ => return HttpResponse::InternalServerError().finish(),
    };
    return HttpResponse::Ok().json(TokenResult {
        status: Status::Success,
        message: fl!(loader, "token-exchanged", email = user.email),
        token: auth_token,
    });
}

#[cfg(test)]
mod exchange_token {
    use super::*;
    use crate::config;
    use actix_web::{http::StatusCode, test, web, App};

    #[actix_rt::test]
    async fn gets_200_if_token_is_exchanged() {
        let db_pool = PgPool::connect(&config::CONFIG.database_url).await.unwrap();
        let user = models::User::create("gets_200_if_token_is_exchanged@example.com", &db_pool)
            .await
            .expect("can't create user");
        let token_promise = models::TokenPromise::create(user.id, &db_pool)
            .await
            .unwrap();

        let mut app = test::init_service(
            App::new()
                .data(db_pool.clone())
                .route("/", web::post().to(exchange_token)),
        )
        .await;
        let req = test::TestRequest::post()
            .uri("/")
            .set_json(&ExchangeData {
                user_id: user.id,
                token: token_promise.content.clone(),
            })
            .to_request();

        let response = test::call_service(&mut app, req).await;

        assert_eq!(response.status(), StatusCode::OK);
        let body = test::read_body(response).await;
        let result: ApiResult = serde_json::from_slice(&body)
            .expect("read_response_json failed during deserialization");
        assert!(matches!(result.status, Status::Success));
    }

    #[actix_rt::test]
    async fn gets_400_when_token_not_recognized() {
        let db_pool = PgPool::connect(&config::CONFIG.database_url).await.unwrap();
        let user = models::User::create("gets_400_when_token_not_recognized@example.com", &db_pool)
            .await
            .expect("can't create user");
        let mut app = test::init_service(
            App::new()
                .data(PgPool::connect(&config::CONFIG.database_url).await.unwrap())
                .route("/", web::post().to(exchange_token)),
        )
        .await;
        let req = test::TestRequest::post()
            .uri("/")
            .set_json(&ExchangeData {
                user_id: user.id,
                token: "some-token00000000000000000000000000".to_string(),
            })
            .to_request();

        let response = test::call_service(&mut app, req).await;

        assert_eq!(response.status(), StatusCode::BAD_REQUEST);
        let body = test::read_body(response).await;
        let result: ApiResult = serde_json::from_slice(&body)
            .expect("read_response_json failed during deserialization");
        assert_eq!(
            result,
            ApiResult {
                status: Status::Error,
                message: "Data not found for received email & token".to_string(),
            }
        );
    }

    #[actix_rt::test]
    async fn gets_400_if_token_already_used() {
        let db_pool = PgPool::connect(&config::CONFIG.database_url).await.unwrap();
        let user = models::User::create("gets_400_if_token_already_used@example.com", &db_pool)
            .await
            .expect("can't create user");
        let token_promise = models::TokenPromise::create(user.id, &db_pool)
            .await
            .unwrap();
        let mut app = test::init_service(
            App::new()
                .data(db_pool.clone())
                .route("/", web::post().to(exchange_token)),
        )
        .await;
        let use_token = test::TestRequest::post()
            .uri("/")
            .set_json(&ExchangeData {
                user_id: user.id,
                token: token_promise.content.clone(),
            })
            .to_request();
        let response = test::call_service(&mut app, use_token).await;
        assert_eq!(response.status(), StatusCode::OK);

        let reuse_token = test::TestRequest::post()
            .uri("/")
            .set_json(&ExchangeData {
                user_id: user.id,
                token: token_promise.content.clone(),
            })
            .to_request();
        let response = test::call_service(&mut app, reuse_token).await;
        assert_eq!(response.status(), StatusCode::BAD_REQUEST);

        let body = test::read_body(response).await;
        let result: ApiResult = serde_json::from_slice(&body)
            .expect("read_response_json failed during deserialization");
        assert_eq!(
            result,
            ApiResult {
                status: Status::Error,
                message: "Token invalid - already used".to_string(),
            }
        );
    }
}
