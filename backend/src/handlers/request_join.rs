use super::ApiResult;
use super::Status;
use actix_web::{web, HttpResponse, Responder};
use i18n_embed::fluent::FluentLanguageLoader;
use i18n_embed_fl::fl;
use serde::{Deserialize, Serialize};
use sqlx::PgPool;
use validator::Validate;

use crate::config;
use crate::i18n;
use crate::models;
use crate::utils;

#[derive(Debug, Deserialize, Serialize, Validate)]
pub struct JoinRequest {
    #[validate(email)]
    pub email: String,
}

pub async fn request_join(
    wrapper: i18n::LoaderWrapper,
    join_data: web::Json<JoinRequest>,
    db_pool: web::Data<PgPool>,
) -> impl Responder {
    let loader: FluentLanguageLoader = wrapper.into();
    if let Err(e) = join_data.validate() {
        eprintln!("{:?}", e);
        return HttpResponse::BadRequest().json(ApiResult {
            status: Status::Error,
            // TODO: https://github.com/projectfluent/fluent-rs/pull/116/files
            message: fl!(
                loader,
                "invalid-email",
                email = format!("{:?}", join_data.email),
            ),
        });
    }

    let user = match models::User::get_by_email(&join_data.email, &db_pool).await {
        Ok(Some(user)) => user,
        Ok(None) => match models::User::create(&join_data.email, &db_pool).await {
            Ok(user) => user,
            Err(e) => {
                eprintln!("New user failure {:?}", e);
                return HttpResponse::InternalServerError().json(ApiResult {
                    status: Status::Error,
                    message: fl!(loader, "generic-error-msg"),
                });
            }
        },
        Err(e) => {
            eprintln!("{:?}", e);
            return HttpResponse::InternalServerError().json(ApiResult {
                status: Status::Error,
                message: fl!(loader, "generic-error-msg"),
            });
        }
    };

    let result = models::TokenPromise::create(user.id, &db_pool).await;
    let token_promise = match result {
        Ok(token_promise) => token_promise,
        Err(e) => {
            eprintln!("{:?}", e);
            return HttpResponse::InternalServerError().finish();
        }
    };
    let reset_link = format!(
        "{app_address}/ExchangeToken?user_id={user_id}&token={token}",
        app_address = config::CONFIG.app_address,
        user_id = user.id,
        token = token_promise.content,
    );
    if let Err(e) = utils::send_email(
        &join_data.email,
        &fl!(loader, "join-email-title"),
        &fl!(loader, "join-email-body", reset_link = reset_link.as_str()),
        config::CONFIG.skip_email,
    ) {
        eprintln!("{:?}", e);
        return HttpResponse::InternalServerError().json(ApiResult {
            status: Status::Error,
            message: fl!(loader, "cant-send-join-email"),
        });
    }
    HttpResponse::Ok().json(ApiResult {
        status: Status::Success,
        message: fl!(loader, "join-email-sent", email = join_data.email.as_str()),
    })
}

#[cfg(test)]
mod test_request_join {
    use super::*;
    use crate::config;
    use actix_web::{http::StatusCode, test, web, App};

    #[actix_rt::test]
    async fn works_if_email_ok() {
        let db_pool = PgPool::connect(&config::CONFIG.database_url).await.unwrap();
        let user = models::User::create("works_if_email_ok@example.com", &db_pool)
            .await
            .expect("can't create user");
        let mut app = test::init_service(
            App::new()
                .data(PgPool::connect(&config::CONFIG.database_url).await.unwrap())
                .route("/", web::post().to(request_join)),
        )
        .await;
        let req = test::TestRequest::post()
            .uri("/")
            .set_json(&JoinRequest {
                email: user.email.clone(),
            })
            .to_request();

        let response = test::call_service(&mut app, req).await;

        assert_eq!(response.status(), StatusCode::OK);
        let body = test::read_body(response).await;
        let result: ApiResult = serde_json::from_slice(&body)
            .expect("read_response_json failed during deserialization");
        assert_eq!(
            result,
            ApiResult {
                status: Status::Success,
                message: format!(
                    "The Join Email was just sent to: \u{2068}{}\u{2069}",
                    user.email
                ),
            }
        );
    }

    #[actix_rt::test]
    async fn gets_error_if_email_invalid() {
        let mut app = test::init_service(
            App::new()
                .data(PgPool::connect(&config::CONFIG.database_url).await.unwrap())
                .route("/", web::post().to(request_join)),
        )
        .await;
        let data = JoinRequest {
            email: "invalid".to_string(),
        };

        let req = test::TestRequest::post()
            .uri("/")
            .set_json(&data)
            .to_request();

        let response = test::call_service(&mut app, req).await;

        assert_eq!(response.status(), StatusCode::BAD_REQUEST);
        let body = test::read_body(response).await;
        let result: ApiResult = serde_json::from_slice(&body)
            .expect("read_response_json failed during deserialization");
        assert_eq!(
            result,
            ApiResult {
                status: Status::Error,
                message: "Invalid email, found \u{2068}\"invalid\"\u{2069}".to_string(),
            }
        );
    }

    #[actix_rt::test]
    async fn gets_200_if_user_missing() {
        let mut app = test::init_service(
            App::new()
                .data(PgPool::connect(&config::CONFIG.database_url).await.unwrap())
                .route("/", web::post().to(request_join)),
        )
        .await;
        let email = "some-unknown-user@example.com";
        let req = test::TestRequest::post()
            .uri("/")
            .set_json(&JoinRequest {
                email: email.to_string(),
            })
            .to_request();

        let response = test::call_service(&mut app, req).await;

        assert_eq!(response.status(), StatusCode::OK);
        let body = test::read_body(response).await;
        let result: ApiResult = serde_json::from_slice(&body)
            .expect("read_response_json failed during deserialization");
        assert_eq!(
            result,
            ApiResult {
                status: Status::Success,
                message: format!("The Join Email was just sent to: \u{2068}{}\u{2069}", email),
            }
        );
    }
}
