use actix_web::{web, HttpResponse, Responder};
use actix_web_httpauth::extractors::bearer::BearerAuth;
use i18n_embed::fluent::FluentLanguageLoader;
use i18n_embed_fl::fl;
use serde::{Deserialize, Serialize};
use sqlx;
use sqlx::PgPool;
use std::collections::HashMap;

use super::ApiResult;
use super::Status;
use crate::auth;
use crate::i18n;
use crate::models;

type UserVeto = HashMap<String, bool>;

#[derive(Debug, Deserialize, Serialize, PartialEq)]
struct UserVetos {
    status: Status,
    message: String,
    user_vetos: UserVeto,
}

pub async fn user_veto_get(
    wrapper: i18n::LoaderWrapper,
    db_pool: web::Data<PgPool>,
    auth: BearerAuth,
) -> impl Responder {
    let loader: FluentLanguageLoader = wrapper.into();
    // TODO:: this seems to be popular pattern (see veto handler) so should be handled smartly
    let user_id = match auth::token2id(auth.token()) {
        Ok(user_id_str) => match user_id_str.parse::<i32>() {
            Ok(user_id) => user_id,
            Err(e) => {
                eprintln!("{:?}", e);
                return HttpResponse::BadRequest().json(ApiResult {
                    status: Status::Error,
                    message: fl!(loader, "token-sub-not-int"),
                });
            }
        },
        Err(e) => {
            eprintln!("{:?}", e);
            return HttpResponse::BadRequest().json(ApiResult {
                status: Status::Error,
                message: fl!(loader, "token-invalid"),
            });
        }
    };
    let mut user_vetos: UserVeto = HashMap::new();
    let veto_votes = match models::VetoVote::user_votes(user_id, &db_pool).await {
        Ok(votes) => votes,
        Err(e) => {
            eprintln!("{:?}", e);
            return HttpResponse::InternalServerError().json(UserVetos {
                status: Status::Error,
                message: fl!(loader, "generic-error-msg"),
                user_vetos,
            });
        }
    };
    dbg!(&user_vetos);
    for veto_vote in veto_votes.into_iter() {
        user_vetos.insert(veto_vote.act_id, true);
    }
    return HttpResponse::Ok().json(UserVetos {
        status: Status::Success,
        message: "".to_string(),
        user_vetos: user_vetos,
    });
}

#[cfg(test)]
mod test_user_veto_get {
    use super::*;
    use crate::config;
    use actix_web::{http::StatusCode, test, web, App};
    use actix_web_httpauth::middleware::HttpAuthentication;

    #[actix_rt::test]
    async fn it_returns_only_user_vetos() {
        let db_pool = PgPool::connect(&config::CONFIG.database_url)
            .await
            .expect("can't connect db");
        let act_id = "some-act-id";
        let user1 = models::User::create("akira@example.com", &db_pool)
            .await
            .expect("can't create user");
        let token = auth::generate_jwt(user1.id.to_string()).expect("can't generate token");
        models::VetoVote::create(user1.id, &act_id, &db_pool)
            .await
            .expect("can't create veto vote for user1");
        let user2 = models::User::create("pai@example.com", &db_pool)
            .await
            .expect("can't create user");
        models::VetoVote::create(user2.id, &act_id, &db_pool)
            .await
            .expect("can't create veto vote for user2");
        let mut app = test::init_service(
            App::new()
                .data(db_pool.clone())
                .wrap(HttpAuthentication::bearer(auth::bearer_validator))
                .route("/", web::get().to(user_veto_get)),
        )
        .await;

        let req = test::TestRequest::get()
            .uri("/")
            .header("Authorization", format!("Bearer {}", &token))
            .to_request();

        let response = test::call_service(&mut app, req).await;
        assert_eq!(response.status(), StatusCode::OK);
        let body = test::read_body(response).await;
        let result: UserVetos = serde_json::from_slice(&body)
            .expect("read_response_json failed during deserialization");
        let mut user_vetos = HashMap::new();
        user_vetos.insert(act_id.to_string(), true);
        assert_eq!(
            result,
            UserVetos {
                status: Status::Success,
                message: "".to_string(),
                user_vetos
            }
        );
    }
}
