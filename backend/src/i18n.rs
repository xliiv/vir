use actix_web::{dev, Error, FromRequest, HttpRequest};
use futures_util::future::{ok, Ready};
use i18n_embed::fluent::{fluent_language_loader, FluentLanguageLoader};
use rust_embed::RustEmbed;
use unic_langid::LanguageIdentifier;

const ACCEPT_LANG: &'static str = "Accept-Language";

#[derive(RustEmbed)]
#[folder = "i18n"] // path to the compiled localization resources
struct Translations;

#[derive(Debug)]
pub struct LoaderWrapper(FluentLanguageLoader);

impl From<LoaderWrapper> for FluentLanguageLoader {
    fn from(i: LoaderWrapper) -> FluentLanguageLoader {
        i.0
    }
}

impl FromRequest for LoaderWrapper {
    type Error = Error;
    type Future = Ready<Result<Self, Self::Error>>;
    type Config = ();

    fn from_request(req: &HttpRequest, _payload: &mut dev::Payload) -> Self::Future {
        let loader: FluentLanguageLoader = fluent_language_loader!();
        let translations = Translations;
        let langs: Vec<LanguageIdentifier> = req
            .headers()
            .get(ACCEPT_LANG)
            .and_then(|v| v.to_str().ok())
            .unwrap_or("en-US")
            .split(",")
            .filter_map(|lang| {
                lang
                    // get "en" from "en-US"
                    .split(|c| c == '-' || c == ';')
                    .nth(0)
                    .map(|locale| LanguageIdentifier::from_bytes(locale.as_bytes()).ok())
                    .flatten()
            })
            .collect();
        if let Err(e) = i18n_embed::select(&loader, &translations, &langs) {
            eprintln!("{:?}", e);
        }
        ok(LoaderWrapper(loader))
        // use actix_web::error::ErrorBadRequest;
        // use futures_util::future::err;
        // err(ErrorBadRequest("no luck"))
    }
}
