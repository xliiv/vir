invalid-email = Invalid email, found {$email}
cant-send-join-email = Can't send the Join email, come back later!
join-email-sent = The Join Email was just sent to: {$email}
join-email-title = Join Link from Chcemywir.pl
join-email-body = Your ONE-TIME SECRET LINK (FOR YOUR USE ONLY) is: {$reset_link}
not-account = You are not using account
account-left = Left your account
veto-submitted = Veto submitted
veto-deleted = Veto canceled
# exchange-token handler
invalid-exchange-token = Token is not 36 chars & email is invalid
cant-generate-token = Token can't be generated
promise-data-missing = Data not found for received email & token
token-used = Token invalid - already used
token-obsolete = Token invalid - got obsolete
cant-get-user = Given user not found
token-exchanged = Congratulation, you are using VIR as {$email}
# veto votes handler
veto-vote-error = Error occured, vote later!
veto-vote-exists = Veto vote exists for user {$user_id} and act {$act_id}
veto-vote-missing = Veto vote missing for user {$user_id} and act {$act_id}
token-sub-not-int = Token sub part is not valid integer
token-invalid = Token is invalid
# misc
generic-error-msg = Error found, come back later!
