invalid-email = Email {$email} ma błedny format
cant-send-join-email = Email nie może być wysłany, wróć później!
join-email-sent = Zaproszenie wysłane na adres: {$email}
join-email-title = Identyfikacja w Chcemywir.pl
join-email-body = Twój jednorazowy, osobisty odnośnik (WYŁĄCZNIE DO TWOJEGO UŻYTKU) to: {$reset_link}
not-account = Nie korzystasz z żadnego konta
account-left = Opuściłeś konto
veto-submitted = Weto przyjęto
veto-deleted = Veto wycofane
# exchange-token handler
invalid-exchange-token = Żeton ma mieć 36 znaków oraz email jest wadliwy
cant-generate-token = Nie można stworzyć żetonu
promise-data-missing = Brak wpisu dla podanych danych
token-used = Wadliwy żeton - był użyty
token-obsolete = Wadliwy żeton - jest zarzucony
cant-get-user = Brak podanego użytkownika
token-exchanged = Gratulacje, używasz WIR jako {$email}
# veto votes handler
veto-vote-error = Wystąpił błąd, zagłosuj później!
veto-vote-exists = Istnieje już weto dla użytkownika {$user_id} i ustawy {$act_id}
veto-vote-missing = Brak weta dla użytkownika {$user_id} i ustawy {$act_id}
token-sub-not-int = Część sub w żetonie ma być liczbą całkowitą
token-invalid = Błędny żeton
# misc
generic-error-msg = Wystąpił błąd, wróć później!
