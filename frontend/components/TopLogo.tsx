import * as Linking from 'expo-linking'
import * as React from 'react'
import Constants from 'expo-constants'
import i18n from '../i18n/main.tsx'
import { FontAwesome5 } from '@expo/vector-icons'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'

export default function TopLogo (props, navigation) {
  return (
    <View style={styles.container}>
      <View style={styles.wrapper}>
        <TouchableOpacity onPress={() => { Linking.openURL('/') }}>
          <Text style={styles.logoText}> {i18n.t('appName')} </Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => { Linking.openURL(i18n.t('wikiUrl')) }}>
          <FontAwesome5 style={styles.infoIcon} name="info-circle" />
        </TouchableOpacity>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  wrapper: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  logoText: {
    fontSize: 26,
    fontWeight: 'bold'
  },
  infoIcon: {
    color: '#FFE51A',
    fontSize: 26
  }
})
