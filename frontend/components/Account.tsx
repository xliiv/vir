import * as React from 'react';
import i18n from '../i18n/main.tsx';
import { Button, StyleSheet, View } from "react-native";

import { AuthContext } from '../navigation';

export default function Account(props, navigation) {
  const accountButtons = (auth) => {
    return (
      auth.token === null ? (
        <View style={styles.button}>
          <Button onPress={() => navigation.navigate('Join')} title={i18n.t("joinButton")}></Button>
        </View>
      ) : (
        <View style={styles.button}>
          <Button onPress={() => navigation.navigate('Leave')} title={i18n.t("leaveButton")}></Button>
        </View>
      )
    );
  };

  return (
    <View style={styles.buttonWrapper}>
      <AuthContext.Consumer>{accountButtons}</AuthContext.Consumer>
    </View>
  );
}

const styles = StyleSheet.create({
  buttonWrapper: {
    flex: 1,
    flexDirection: 'row',
  },
  button: {
    flex: 1,
    alignItems: 'flex-end',
    justifyContent: 'center',
    paddingHorizontal: 20,
  },
});
