export type RootStackParamList = {
  Root: undefined;
  NotFound: undefined;
};

export type BottomTabParamList = {
  Veto: undefined;
  Initiative: undefined;
  Referendum: undefined;
};

export type VetoParamList = {
  VetoScreen: undefined;
};

export type InitiativeParamList = {
  InitiativeScreen: undefined;
};

export type ReferendumParamList = {
  ReferendumScreen: undefined;
}
