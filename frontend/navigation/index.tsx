import AsyncStorage from '@react-native-community/async-storage';
import { NavigationContainer, DefaultTheme, DarkTheme } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import * as React from 'react';
import { ColorSchemeName } from 'react-native';

import { RootStackParamList } from '../types';
import BottomTabNavigator from './BottomTabNavigator';
import LinkingConfiguration from './LinkingConfiguration';
import NotFoundScreen from '../screens/NotFoundScreen';
import JoinScreen from '../screens/JoinScreen.tsx';
import ExchangeTokenScreen from '../screens/ExchangeTokenScreen.tsx';
import LeaveScreen from '../screens/LeaveScreen.tsx';

// https://reactnavigation.org/docs/navigating-without-navigation-prop
export const navigationRef = React.createRef();
export function navigate(name, params) {
  navigationRef.current?.navigate(name, params);
}
export const AuthContext = React.createContext({});

// If you are not familiar with React Navigation, we recommend going through the
// "Fundamentals" guide: https://reactnavigation.org/docs/getting-started
export default function Navigation({ colorScheme }: { colorScheme: ColorSchemeName }) {
  return (
      <NavigationContainer
        linking={LinkingConfiguration}
        ref={navigationRef}
        theme={colorScheme === 'dark' ? DarkTheme : DefaultTheme}>
        <RootNavigator />
      </NavigationContainer>
  );
}

// A root stack navigator is often used for displaying modals on top of all other content
// Read more here: https://reactnavigation.org/docs/modal
const Stack = createStackNavigator<RootStackParamList>();

function RootNavigator({navigation}) {
  const [token, setToken] = React.useState(null);

  const Auth = {
    token,
    setToken,
    tokenValid: (token) => {
      return token != null;
    },
  };

  React.useEffect(() => {
      const bootstrapAsync = async () => {
        if (token === null) {
          const storageToken = await AsyncStorage.getItem('token');
          if (storageToken !== "null") {
            setToken(storageToken);
          }
        }
      };
      bootstrapAsync();
  }, [token]);

  return (
    <AuthContext.Provider value={Auth}>
      <Stack.Navigator screenOptions={{ headerShown: false }}>
        <Stack.Screen name="Root" component={BottomTabNavigator} />
        <Stack.Screen name="Join" component={JoinScreen} />
        <Stack.Screen name="ExchangeToken" component={ExchangeTokenScreen} />
        <Stack.Screen name="Leave" component={LeaveScreen} />
        <Stack.Screen name="NotFound" component={NotFoundScreen} options={{ title: 'Oops!' }} />
      </Stack.Navigator>
    </AuthContext.Provider>
  );
}
