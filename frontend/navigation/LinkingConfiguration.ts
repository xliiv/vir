import * as Linking from 'expo-linking';

export default {
  prefixes: [Linking.makeUrl('/')],
  config: {
    screens: {
      Root: {
        screens: {
          Veto: {
            screens: {
              VetoScreen: 'Weto',
            },
          },
          Initiative: {
            screens: {
              InitiativeScreen: 'Incjatywa',
            },
          },
          Referendum: {
            screens: {
              ReferendumScreen: 'Referendum',
            },
          },
        },
      },
      Join: 'Join',
      ExchangeToken: 'ExchangeToken',
      Leave: 'Leave',
      NotFound: '*',
    },
  },
};
