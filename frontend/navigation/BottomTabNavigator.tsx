import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import * as React from 'react';
import { StyleSheet } from "react-native";

import Account from '../components/Account.tsx';
import Colors from '../constants/Colors';
import VetoScreen from '../screens/VetoScreen';
import ReferendumScreen from '../screens/ReferendumScreen';
import InitiativeScreen from '../screens/InitiativeScreen';
import TopLogo from '../components/TopLogo.tsx';
import i18n from '../i18n/main.tsx';
import useColorScheme from '../hooks/useColorScheme';
import { BottomTabParamList, VetoParamList, InitiativeParamList, ReferendumParamList } from '../types';

const BottomTab = createBottomTabNavigator<BottomTabParamList>();

export default function BottomTabNavigator() {
  const colorScheme = useColorScheme();

  return (
    <BottomTab.Navigator
      initialRouteName="Veto"
      tabBarOptions={{ activeTintColor: Colors[colorScheme].tint }}>
      <BottomTab.Screen
        name="Veto"
        component={VetoNavigator}
        options={{
          tabBarLabel: i18n.t('vetoTitle'),
        }}
      />
      <BottomTab.Screen
        name="Initiative"
        component={InitiativeNavigator}
        options={{
          tabBarLabel: i18n.t('initiativeTitle'),
        }}
      />
      <BottomTab.Screen
        name="Referendum"
        component={ReferendumNavigator}
        options={{
          tabBarLabel: i18n.t('referendumTitle'),
        }}
      />
    </BottomTab.Navigator>
  );
}

// Each tab has its own navigation stack, you can read more about this pattern here:
// https://reactnavigation.org/docs/tab-based-navigation#a-stack-navigator-for-each-tab
const VetoStack = createStackNavigator<VetoParamList>();

function VetoNavigator({navigation}) {
  return (
    <VetoStack.Navigator>
      <VetoStack.Screen
        name="VetoScreen"
        component={VetoScreen}
        options={({ navigation, route }) => ({
          title: i18n.t('vetoTitle'),
          headerTitle: (props) => TopLogo(props, navigation),
          headerRight: (props) => Account(props, navigation),
          headerTitleAlign: "center",
        })}
      />
    </VetoStack.Navigator>
  );
}

const InitiativeStack = createStackNavigator<InitiativeParamList>();

function InitiativeNavigator() {
  return (
    <InitiativeStack.Navigator>
      <InitiativeStack.Screen
        name="InitiativeScreen"
        component={InitiativeScreen}
        options={({ navigation, route }) => ({
          title: i18n.t('initiativeTitle'),
          headerTitle: (props) => TopLogo(props, navigation),
          headerRight: (props) => Account(props, navigation),
          headerTitleAlign: "center",
        })}
      />
    </InitiativeStack.Navigator>
  );
}

const ReferendumStack = createStackNavigator<ReferendumParamList>();

function ReferendumNavigator() {
  return (
    <ReferendumStack.Navigator>
      <ReferendumStack.Screen
        name="ReferendumScreen"
        component={ReferendumScreen}
        options={({ navigation, route }) => ({
          title: i18n.t('referendumTitle'),
          headerTitle: (props) => TopLogo(props, navigation),
          headerRight: (props) => Account(props, navigation),
          headerTitleAlign: "center",
        })}
      />
    </ReferendumStack.Navigator>
  )
}
