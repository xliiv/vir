import 'dotenv/config';

export default ({ config }) => {
  return {
    extra: {
      actAPIAddress: process.env.VIR_ACT_API_ADDRESS,
      backendAddress: process.env.VIR_BACKEND_ADDRESS,
      infoPageAddress: process.env.VIR_INFO_PAGE_ADDRESS,
    },
    ...config,
  };
};
