# Dev

```shell
yarn install
expo start:web
```


# Docker

## Building

```shell
docker build . --build-arg NODE_VERSION=12-alpine --build-arg EXPO_VERSION=3 -t vir-front
```


## Running

```shell
docker run --network host --rm vir-front
```
