import * as Localization from 'expo-localization';
import i18n from 'i18n-js';

i18n.translations = {
  en: {
    appName: 'VIR',
    // Auth screens
    joinButton: 'Join',
    accountLeft: 'Left your account',
    // Veto screen
    vetoTitle: 'Veto',
    vetoButton: 'Veto',
    unvetoButton: 'Unveto',
    prevButton: 'Previous',
    nextButton: 'Next',
    // Initiative screen
    initiativeTitle: 'Initiative',
    initiativeText: 'We are working on supporting Initiative. Be patient :)',
    // Referendum screen
    referendumTitle: 'Referendum',
    referendumText: 'We are working on supporting Referendum. Be patient :)',
    // NotFound Screen
    notFoundTitle: "This screen doesn't exist.",
    notFoundGoHome: 'Go to home screen!',
    // misc
    info: 'Info',
    networkError: 'An error occured, try later - thanks!',
    leaveButton: "Leave",
    wikiUrl: "https://gitlab.com/xliiv/vir/-/blob/master/wiki/en/README.md#vir-concept",
  },
  pl: {
    appName: 'WIR',
    // Auth screens
    joinButton: 'Dołącz',
    accountLeft: 'Opuściłeś konto',
    // Veto screen
    vetoTitle: 'Weto',
    vetoButton: 'Wetuj',
    unvetoButton: 'Odwetuj',
    prevButton: 'Wstecz',
    nextButton: 'Dalej',
    // Initiative screen
    initiativeTitle: 'Inicjatywy',
    initiativeText: 'Pracujemy nad obsługą Inicjatyw Obywatelskich. Bądź cierpliwy :)',
    // Referendum screen
    referendumTitle: 'Referendum',
    referendumText: 'Pracujemy nad obsługą Referendów. Bądź cierpliwy :)',
    // NotFound Screen
    notFoundTitle: "Nie odnaleziono",
    notFoundGoHome: 'Przejdź do ekranu Głównego',
    // misc
    info: 'Info',
    networkError: 'Trafił się błąd, wróć póżniej - dzięki!',
    leaveButton: "Opuść Konto",
    wikiUrl: "https://gitlab.com/xliiv/vir/-/blob/master/wiki/pl/README.md#koncepcja-wir",
  },
};

i18n.locale = Localization.locale;
i18n.fallbacks = true;

export default i18n;
