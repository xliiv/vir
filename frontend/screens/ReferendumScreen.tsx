import * as React from 'react'
import i18n from '../i18n/main.tsx'
import { ActivityIndicator, Button, FlatList, StyleSheet } from 'react-native'
import { Text, View } from '../components/Themed'

export default function ReferendumScreen() {
  return (
    <View style={styles.infoContainer}>
      <Text style={styles.title}>{i18n.t('referendumText')}</Text>
    </View>
  )
}

const styles = StyleSheet.create({
  infoContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold'
  }
})
