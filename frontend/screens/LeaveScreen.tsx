import * as Localization from 'expo-localization'
import AsyncStorage from '@react-native-community/async-storage'
import Constants from 'expo-constants'
import React from 'react'
import i18n from '../i18n/main.tsx'
import { View } from 'react-native'

import { AuthContext } from '../navigation'

const LeaveScreen = ({ navigation }) => {
  const { token, setToken } = React.useContext(AuthContext)

  React.useEffect(() => {
    const leave = async () => {
      if (token === null) {
        navigation.navigate('Root')
        return
      }
      // always clear the client token, it's not ideal but imagine this case
      // 1. a user logs in using a public computer
      // 2. they click logout
      // 3. networks failure happens
      // 4. they think logout is successful and leave
      // 5. but in fact they leave the computer with authenticated session
      // so remvoe client token ahead of time
      const _ = await AsyncStorage.setItem('token', null)
      setToken(null)
      const response = fetch(`${Constants.manifest.extra.backendAddress}/auth/leave`, {
        method: 'POST',
        headers: {
          'Accept-Language': Localization.locale,
          Authorization: 'Bearer ' + token,
          'Content-Type': 'application/json'
        }
      })
        .finally(() => {
          alert(i18n.t('accountLeft'))
          navigation.navigate('Root')
        })
    }
    leave()
  }, [token, setToken, navigation])

  return <></>
}

export default LeaveScreen
