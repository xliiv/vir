import * as Localization from 'expo-localization'
import AsyncStorage from '@react-native-community/async-storage'
import Constants from 'expo-constants'
import React from 'react'
import i18n from '../i18n/main.tsx'
import { View } from 'react-native'

import { AuthContext } from '../navigation'

const resetNavigate = (navigation, screenName) => {
  navigation.reset({
    index: 0,
    routes: [{ name: screenName }]
  })
}

const ExchangeTokenScreen = ({ route, navigation }) => {
  const urlToken = route.params?.token
  const user_id = route.params?.user_id
  if (urlToken == null || user_id == null) {
    resetNavigate(navigation, 'Root')
  }
  const { setToken } = React.useContext(AuthContext)

  React.useEffect(() => {
    const exchangeToken = async () => {
      await fetch(
        `${Constants.manifest.extra.backendAddress}/auth/exchange-token`, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            'Accept-Language': Localization.locale
          },
          body: JSON.stringify({
            user_id: parseInt(user_id, 10),
            token: urlToken
          })
        })
        .then((response) => response.json())
        .then(async (json) => {
          if (json.status.toLowerCase() === 'success') {
            const fetchedToken = json.token
            await AsyncStorage.setItem('token', fetchedToken)
            setToken(fetchedToken)
            alert(json.message)
            resetNavigate(navigation, 'Root')
          } else {
            alert(json.message)
          }
        })
        .catch((error) => {
          console.log(error)
        })
    }
    exchangeToken()
  }, [setToken, user_id, urlToken, navigation])

  resetNavigate(navigation, 'Root')

  return <></>
}

export default ExchangeTokenScreen
