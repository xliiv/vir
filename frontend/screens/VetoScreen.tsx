import * as Localization from 'expo-localization'
import * as React from 'react'
import Constants from 'expo-constants'
import i18n from '../i18n/main.tsx'
import { ActivityIndicator, Button, FlatList, StyleSheet } from 'react-native'
import { Text, View } from '../components/Themed'
import { AuthContext } from '../navigation'

function showJoinIf401 (response, navigation) {
  if (response.status === 401) {
    navigation.navigate('Join')
  }
  return response.json()
};

export default function VetoScreen({ route, navigation }) {
  const [loading, setLoading] = React.useState(false)
  const [acts, setActs] = React.useState([])
  const [publisher, setPublisher] = React.useState('DU')
  const [year, setYear] = React.useState('2020')
  const [offset, setOffset] = React.useState(0)
  const [limit, setLimit] = React.useState(10)
  const [total, setTotal] = React.useState(0)
  const [canPrevious, setCanPrevious] = React.useState(false)
  const [canNext, setCanNext] = React.useState(false)
  const [type, setType] = React.useState('Ustawa')
  const [userVetos, setUserVetos] = React.useState({})

  const { token } = React.useContext(AuthContext)

  React.useEffect(() => {
    if (token === null) return
    fetch(`${Constants.manifest.extra.backendAddress}/api/user-veto`, {
      headers: {
        'Accept-Language': Localization.locale,
        Authorization: 'Bearer ' + token,
        'Content-Type': 'application/json'
      }
    })
      .then(response => showJoinIf401(response, navigation))
      .then(json => {
        if (json.status.toLowerCase() === 'success') {
          setUserVetos(json.user_vetos)
        }
      })
      .catch((error) => {
        console.error(error)
      })

    // Run clean up after useEffect
    return () => setUserVetos({})
  }, [token, navigation])

  React.useEffect(() => {
    setLoading(true)
    fetch(`${Constants.manifest.extra.actAPIAddress}/eli/acts/search?publisher=${publisher}&year=${year}&offset=${offset}&limit=${limit}&type=${type}`)
      .then(response => response.json())
      .then(json => {
        setLoading(false)
        setTotal(json.totalCount)
        setCanNext(offset + limit < json.totalCount)
        setCanPrevious(offset - limit >= 0)
        setActs([...json.items])
      })
      .catch((error) => {
        console.error(error)
      })
  }, [publisher, year, offset, limit, type])

  const loadPrevious = () => {
    setOffset(offset - limit)
  }
  const loadNext = () => {
    setOffset(offset + limit)
  }

  const veto = async (actId) => {
    if (token === null) {
      navigation.navigate('Join')
      return
    }
    await fetch(`${Constants.manifest.extra.backendAddress}/api/veto`, {
      method: 'POST',
      headers: {
        'Accept-Language': Localization.locale,
        Authorization: 'Bearer ' + token,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        act_id: actId
      })
    })
      .then(response => showJoinIf401(response, navigation))
      .then(json => {
        alert(json.message)
        if (json.status.toLowerCase() === 'success') {
          const vetoCopy = Object.assign({}, userVetos)
          vetoCopy[actId] = true
          setUserVetos(vetoCopy)
        }
      })
      .catch((error) => {
        console.error(error)
      })
  }
  const unveto = async (actId) => {
    if (token === null) {
      navigation.navigate('Join')
      return
    }
    await fetch(`${Constants.manifest.extra.backendAddress}/api/user-veto`, {
      method: 'DELETE',
      headers: {
        'Accept-Language': Localization.locale,
        Authorization: 'Bearer ' + token,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        act_id: actId
      })
    })
      .then(response => showJoinIf401(response, navigation))
      .then(json => {
        alert(json.message)
        if (json.status.toLowerCase() === 'success') {
          const vetoCopy = Object.assign({}, userVetos)
          vetoCopy[actId] = false
          setUserVetos(vetoCopy)
        }
      })
      .catch((error) => {
        console.error(error)
      })
  }

  const renderItem =
    ({ item }) => {
      return (
        <View style={styles.listItem}>
          <View style={styles.titleView} >
            <Text style={styles.actTitle}>{item.title}</Text>
          </View>
          <View style={styles.ActionView} >
            { userVetos[item.ELI] === true ? (
              <Button onPress={() => unveto(item.ELI)} title={i18n.t('unvetoButton')}></Button>
            ) : (
              <Button onPress={() => veto(item.ELI)} title={i18n.t('vetoButton')}></Button>
            )}
          </View>
        </View>
      )
    }

  return (
    <View style={styles.rootConatainer}>
      <FlatList
        data={acts}
        keyExtractor={item => item.ELI}
        renderItem={renderItem}
        ListFooterComponent={
          loading ? (
            <ActivityIndicator />
          ) : (
            <View style={styles.navigationButtons}>
              <Button disabled={!canPrevious} title={i18n.t('prevButton')} onPress={loadPrevious} />
              <Button disabled={!canNext} title={i18n.t('nextButton')} onPress={loadNext} />
            </View>
          )
        }
      />
    </View>
  )
}

const styles = StyleSheet.create({
  rootConatainer: {
    flex: 1,
    justifyContent: 'center'
  },
  listItem: {
    flex: 1,
    flexDirection: 'row'
  },
  titleView: {
    flex: 8,
    alignItems: 'flex-start'
  },
  ActionView: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  actTitle: {
    padding: 10,
    fontSize: 18
  },
  navigationButtons: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center'
  }
})
