import * as Localization from 'expo-localization'
import AsyncStorage from '@react-native-community/async-storage'
import Constants from 'expo-constants'
import React from 'react'
import i18n from '../i18n/main.tsx'
import { StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native'

const JoinScreen = ({ navigation }) => {
  const [email, setEmail] = React.useState('')

  const join = async () => {
    await fetch(
      `${Constants.manifest.extra.backendAddress}/auth/join`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Accept-Language': Localization.locale
        },
        body: JSON.stringify({
          email: email
        })
      })
      .then((response) => response.json())
      .then((json) => {
        alert(json.message)
      })
      .catch((error) => {
        console.log(error)
      })
  }

  return (
    <View style={styles.container}>
      <Text style={styles.logo}>{i18n.t('appName')}</Text>
      <View style={styles.inputView} >
        <TextInput
          style={styles.inputText}
          autoCompleteType="email"
          placeholder="Email"
          value={email}
          onChangeText={setEmail}
        />
      </View>
      <TouchableOpacity style={styles.mainButton} onPress={join}>
        <Text style={styles.mainButtonText}>{i18n.t('joinButton')}</Text>
      </TouchableOpacity>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  logo: {
    fontWeight: 'bold',
    fontSize: 50,
    marginBottom: 20
  },
  inputView: {
    width: '80%',
    borderRadius: 25,
    height: 50,
    marginBottom: 20,
    justifyContent: 'center',
    padding: 20
  },
  inputText: {
    height: 50
  },
  mainButton: {
    width: '80%',
    backgroundColor: '#2196f3',
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 20,
    marginBottom: 10
  },
  mainButtonText: {
    color: 'white'
  }
})

export default JoinScreen
