import * as React from 'react'
import { ActivityIndicator, Button, FlatList, StyleSheet } from 'react-native'

// TODO: rm it and from Vetoscreen
import EditScreenInfo from '../components/EditScreenInfo'
import { Text, View } from '../components/Themed'
import i18n from '../i18n/main.tsx'

export default function InitiativeScreen() {
  return (
    <View style={styles.infoContainer}>
      <Text style={styles.title}>{i18n.t('initiativeText')}</Text>
    </View>
  )
}

const styles = StyleSheet.create({
  infoContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold'
  }
})
